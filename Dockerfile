FROM node:21-alpine3.18
WORKDIR /usr/src/app
COPY index.js package.json package-lock.json ./
RUN npm install --only=prod
EXPOSE 80
CMD [ "npm", "run", "index" ]