const express = require("express");
const { Pool } = require("pg");

const pool = new Pool({
  host: process.env.HOST,
  port: process.env.PORT,
  user: process.env.USER,
  password: process.env.PASS,
  database: process.env.DB,
});

const app = express();

const appPort = 80;

app.get("/", async (req, res) => {
  const con = await pool.connect();
  // schema_name.table_name
  const users = await con.query("SELECT * FROM users.users");

  const response = { rows: users.rows };

  res.status(200).send(response);
});

app.listen(appPort, () => {
  console.log(`Express running at port ${appPort}`);
});
