
-- Create new schema
CREATE SCHEMA IF NOT EXISTS users;
CREATE TABLE IF NOT EXISTS users.users(
    id INT PRIMARY KEY,
    name VARCHAR(50)
);

-- Insert some placeholder users
INSERT INTO users.users (id, name) VALUES (1, 'John'), (2, 'Jane');
