#!/bin/bash

# next connect to db via:
# 
# LOCAL_SSH_TUNNEL_PORT="5555" \
# PGPASSWORD=password123 psql -h localhost -p $LOCAL_SSH_TUNNEL_PORT -U root

NODE_IP=$(minikube ip)
PG_POD_IP=$(kubectl get svc pg-svc -o jsonpath="{.spec.clusterIP}")

LOCAL_SSH_TUNNEL_PORT="5555"

ssh -i ~/.minikube/machines/minikube/id_rsa \
    -L localhost:$LOCAL_SSH_TUNNEL_PORT:$PG_POD_IP:5432 \
    docker@$NODE_IP