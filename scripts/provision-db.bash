#!/bin/bash

# Usage:
# PGPASSWORD=password123 PGUSER=root ./provision-db.bash [proxy-port]

if [ -z "$PGPASSWORD" ] || [ -z "$PGUSER" ]; then
    echo "Please specify PGUSER and PGPASSWORD before running the script."
    exit 1
fi

# https://stackoverflow.com/a/246128
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

HOST_PORT=46128 # some random value
if [ ! -z "$1" ]; then
    HOST_PORT=$1
fi
echo $HOST_PORT
kubectl port-forward svc/pg-svc $HOST_PORT:5432 &

echo "Wait till port forward succeeds"
sleep 2 # wacky but simplest way

NODE_PASSWORD=node-password123

# idk how to create user with createuser without interactive prompt, so...
psql -h localhost -p $HOST_PORT -U $PGUSER \
    -c "CREATE USER node WITH PASSWORD '$NODE_PASSWORD';" \
    -c "CREATE DATABASE node WITH OWNER='node';"
PGPASSWORD=$NODE_PASSWORD psql -h localhost -p $HOST_PORT -U node -f $SCRIPT_DIR/init-schema.sql

# https://stackoverflow.com/a/1624730
kill %% # kill last bg process

echo NODE_PASSWORD=$NODE_PASSWORD
echo done.